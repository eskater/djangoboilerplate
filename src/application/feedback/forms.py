#coding: utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _

from models import *

class EntryForm(forms.ModelForm):
	picture = forms.ImageField(required=False, label=_(u'Изображение'))

	class Meta:
		model = Entry
		exclude = ['picture', 'moderated']
		widgets = {
			'birthday': forms.TextInput(attrs={'placeholder': 'dd.mm.yyyy'})
		}

	class Media:
		js = ('js/feedback/form.js',)
		js = ('css/feedback/form.css',)