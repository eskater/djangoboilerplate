#coding: utf-8
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.views.generic import FormView

from forms import *
from models import *

from application.main.models import File

class FeedbackView(FormView):
	form_class = EntryForm
	template_name = 'feedback/index.html'

	def get(self, request, *args, **kwargs):
		context = self.get_context_data()

		context['form'] = self.get_form()

		return render(request, 'feedback/index.html', context)

	def post(self, request, *args, **kwargs):
		form = self.get_form()
		context = self.get_context_data()

		if form.is_valid():
			instance = form.save(commit=False)

			if form.cleaned_data['picture']:
				picture = File(file=form.cleaned_data['picture'])
				picture.save()

				instance.picture = picture

			instance.save()

			if instance.pk:
				context['success'] = True

		return render(request, 'feedback/index.html', context)

	def get_initial(self):
		if self.request.user.is_authenticated():
			return {'name': self.request.user.first_name, 'email': self.request.user.email}

		return {}

	def get_context_data(self, **kwargs):
		paginator = Paginator(Entry.objects.filter(moderated=True), 2)

		try:
			kwargs['list'] = paginator.page(self.request.GET.get('page'))
		except PageNotAnInteger:
			kwargs['list'] = paginator.page(1)
		except EmptyPage:
			kwargs['list'] = paginator.page(paginator.num_pages)

		return super(FeedbackView, self).get_context_data(**kwargs)
