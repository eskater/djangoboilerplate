#coding: utf-8
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin

from models import *

@admin.register(Entry)
class EntryAdmin(admin.ModelAdmin):
    actions = ['moderated_yes', 'moderated_no']
    list_filter = ['moderated', 'date_create']
    list_display = ['id', 'name', 'email', 'moderated', 'date_create']
    list_editable = ['email', 'moderated']
    list_display_links = ['id', 'name']
    search_fields = ['name', 'email']

    def moderated_yes(self, request, queryset):
        for feedback in queryset:
            feedback.moderated = True
            feedback.save()

        self.message_user(request, _(u'Выбранные записи были опубликованы на сайте'))

    moderated_yes.short_description = _(u'Опубликовать записи')

    def moderated_no(self, request, queryset):
        for feedback in queryset:
            feedback.moderated = False
            feedback.save()

        self.message_user(request, _(u'Выбранные записи были деактивированы'))

    moderated_no.short_description = _(u'Деактивировать записи')