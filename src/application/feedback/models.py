#coding: utf-8
import logging

from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from application.main.models import File

class Entry(models.Model):
	name = models.CharField(max_length=30, verbose_name=_(u'Имя'))
	email = models.EmailField(verbose_name=_(u'E-Mail'))
	message = models.TextField(verbose_name=_(u'Сообщение'))
	birthday = models.DateField(null=True, blank=True, verbose_name=_(u'Дата рождения'))
	picture = models.ForeignKey(File, null=True, blank=True, related_name='%(app_label)s_entries', on_delete=models.SET_NULL, verbose_name=_(u'Изображение'))
	moderated = models.BooleanField(default=False, verbose_name=_(u'Проверен'))
	date_modify = models.DateTimeField(auto_now=True, editable=False, verbose_name=_(u'Дата изменения'))
	date_create = models.DateTimeField(auto_now_add=True, verbose_name=_(u'Дата создания'))
	
	class Meta:
		verbose_name = _(u'Запись')
		verbose_name_plural = _(u'Записи')
		ordering = ['-date_create']
	
	def __unicode__(self):
		return self.name

@receiver(pre_save, sender=Entry)
def set_name_auto(sender, instance, **kwargs):
	logging.debug('set_name_auto')

	instance.name = 'Hello World'