# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from application.main.models import User

# Create your models here.
class Blog(models.Model):
    title = models.CharField(max_length=100, verbose_name=_(u'Наименование'))
    slug = models.SlugField(unique=True, max_length=32, verbose_name=_(u'Адресная строка'))
    description = models.TextField(verbose_name=_(u'Описание'))
    author = models.OneToOneField(User, unique=True, verbose_name=_(u'Автор'))
    date_modify = models.DateTimeField(auto_now=True, editable=False, verbose_name=_(u'Дата изменения'))
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_(u'Дата создания'))

    class Meta:
        verbose_name = _(u'Блог')
        verbose_name_plural = _(u'Блоги')
        ordering = ['-date_create']

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:detail', kwargs={'slug': self.slug})

class Entry(models.Model):
    title = models.CharField(max_length=100, verbose_name=_(u'Наименование'))
    content = models.TextField(verbose_name=_(u'Содержимое'))
    blog = models.ForeignKey(Blog, related_name='entries', on_delete=models.CASCADE, verbose_name=_(u'Блог'))
    date_modify = models.DateTimeField(auto_now=True, editable=False, verbose_name=_(u'Дата изменения'))
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_(u'Дата создания'))

    class Meta:
        verbose_name = _(u'Запись')
        verbose_name_plural = _(u'Записи')
        ordering = ['-date_create']

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:entry.detail', kwargs={'pk': self.pk, 'slug': self.blog.slug})
