# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import redirect
from django.views.generic import View, ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from forms import *
from models import *

# Create your views here.
class BlogMixinView(View):
    def get_object(self, queryset=None):
        return self.model.objects.get(author=self.request.user)

    def get_success_url(self):
        return reverse('blog:detail', kwargs={'slug': self.object.slug})

class EntryMixinView(View):
    def get_success_url(self):
        return reverse('blog:entry.list', kwargs={'slug': self.object.blog.slug})

    def get_context_data(self, **kwargs):
        data = super(EntryMixinView, self).get_context_data(**kwargs)

        if data.get('entry') and not data.get('blog'):
            data['blog'] = data.get('entry').blog

        return data

class EntryModifyMixinView(View):
    def get_queryset(self):
        return self.model.objects.filter(blog__author=self.request.user)

class BlogList(ListView):
    model = Blog
    template_name = 'blog/index.html'

class BlogDetail(DetailView):
    model = Blog
    template_name = 'blog/detail.html'

class BlogCreate(BlogMixinView, CreateView):
    model = Blog
    form_class = BlogForm
    template_name = 'blog/create.html'

    def get(self, request, *args, **kwargs):
        items = self.model.objects.filter(author=request.user)

        if items.exists():
            item = items[0]

            return redirect('blog:detail', slug=item.slug)

        return super(BlogCreate, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        blog = form.save(commit=False)
        blog.author = self.request.user

        return super(BlogCreate, self).form_valid(form)

class BlogUpdate(BlogMixinView, UpdateView):
    model = Blog
    form_class = BlogForm
    template_name = 'blog/update.html'

class BlogDelete(BlogMixinView, DeleteView):
    model = Blog
    form_class = BlogForm
    template_name = 'blog/update.html'

class EntryList(ListView):
    model = Entry
    template_name = 'blog/entry/list.html'
    paginate_by = 5

    def get_queryset(self):
        return Entry.objects.filter(blog__slug=self.kwargs['slug'])

    def get_context_data(self, **kwargs):
        data = super(EntryList, self).get_context_data(**kwargs)
        data['blog'] = Blog.objects.get(slug=self.kwargs['slug'])

        return data

class EntryDetail(EntryMixinView, DetailView):
    model = Entry
    template_name = 'blog/entry/detail.html'

class EntryCreate(EntryMixinView, CreateView):
    model = Entry
    form_class = EntryForm
    template_name = 'blog/entry/create.html'

    def form_valid(self, form):
        entry = form.save(commit=False)
        entry.blog = Blog.objects.get(author=self.request.user)

        return super(EntryCreate, self).form_valid(form)

class EntryUpdate(EntryModifyMixinView, EntryMixinView, UpdateView):
    model = Entry
    form_class = EntryForm
    template_name = 'blog/entry/update.html'

class EntryDelete(EntryModifyMixinView, EntryMixinView, DeleteView):
    model = Entry
    template_name = 'blog/entry/delete.html'