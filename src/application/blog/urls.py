#coding: utf-8
from django.conf.urls import url

from views import *

urlpatterns = [
    url(r'^$', BlogList.as_view(), name='index'),
    url(r'^create/$', BlogCreate.as_view(), name='create'),
    url(r'^update/$', BlogUpdate.as_view(), name='update'),
    url(r'^(?P<slug>[\w\-]+)/$', BlogDetail.as_view(), name='detail'),
    url(r'^(?P<slug>[\w\-]+)/list/$', EntryList.as_view(), name='entry.list'),
    url(r'^(?P<slug>[\w\-]+)/(?P<pk>\d+)/$', EntryDetail.as_view(), name='entry.detail'),
    url(r'^entry/create/$', EntryCreate.as_view(), name='entry.create'),
    url(r'^entry/update/(?P<pk>\d+)/$', EntryUpdate.as_view(), name='entry.update'),
    url(r'^entry/delete/(?P<pk>\d+)/$', EntryDelete.as_view(), name='entry.delete'),
]