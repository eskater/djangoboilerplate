#coding: utf-8
from django import template
from django.urls import reverse

from ..models import *

register = template.Library()

@register.inclusion_tag('blog/tags/top_create.html', takes_context=True)
def blog_create_top(context):
	show = False
	request = context['request']

	if request.user.is_authenticated():
		object_list = Blog.objects.filter(author=request.user)

		if request.path != reverse('blog:create') and object_list.count() < 1:
			show = True

	return {'show_create': show}

@register.inclusion_tag('blog/tags/sidebar_right_entries.html')
def blog_entries_sidebar_right():
	return {'object_list': Entry.objects.all()[:2]}