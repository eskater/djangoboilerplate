#coding: utf-8
from django import forms

from application.main.forms import WysiwygForm
from models import *

class BlogForm(forms.ModelForm):
    class Meta:
        model = Blog
        fields = ['title', 'slug', 'description']
        widgets = {'description': WysiwygForm()}

class EntryForm(forms.ModelForm):
    class Meta:
        model = Entry
        fields = ['title', 'content']
        widgets = {'content': WysiwygForm(config={'toolbar': [
            ['font', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
                ['insert', ['link', 'picture', 'video', 'hr']],
                    ['para', ['ul', 'ol', 'paragraph']]]})}