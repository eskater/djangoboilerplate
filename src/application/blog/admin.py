# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from models import *

# Register your models here.
@admin.register(Blog)
class BlogAdmin(SummernoteModelAdmin):
    list_filter = ['author', 'date_create']
    list_display = ['id', 'title', 'author', 'date_modify', 'date_create']
    list_display_links = ['id', 'title']
    prepopulated_fields = {'slug': ('title',)}

@admin.register(Entry)
class BlogAdmin(SummernoteModelAdmin):
    list_filter = ['blog', 'date_create']
    list_display = ['id', 'title', 'blog', 'date_modify', 'date_create']
    list_display_links = ['id', 'title']