#coding: utf-8
from logging import debug

from django import forms

from django_summernote.widgets import SummernoteWidget

from models import *

class WysiwygForm(SummernoteWidget):
    default_config = {
        'toolbar': [
            ['font', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
            ['insert', ['link', 'picture']],
        ]
    }

    def __init__(self, attrs=None, config=None):
        self.config = self.default_config.copy()

        if config:
            self.config.update(config)

        super(WysiwygForm, self).__init__(attrs)

    def get_config(self, context):
        context.update(self.config)

        return context

    def template_contexts(self):
        return self.get_config(super(WysiwygForm, self).template_contexts())

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['text', 'object_id', 'content_type']
        widgets = {'text': WysiwygForm(config={'height': 200}), 'object_id': forms.HiddenInput, 'content_type': forms.HiddenInput}