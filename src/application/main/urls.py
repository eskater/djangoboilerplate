"""lesson URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls import include, static, url
from django.conf.urls.i18n import i18n_patterns
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib import admin

from sitetree.sitetreeapp import register_i18n_trees

from views import *
from models import *

urlpatterns = []

for page in Page.objects.all():
    urlpatterns += [url(r'^(?P<url>%s)$' % page.url, PageView.as_view())]

urlpatterns += i18n_patterns(
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^auth/', include('application.auth.urls', app_name='auth', namespace='auth')),
    url(r'^news/', include('application.news.urls', app_name='news', namespace='news')),
    url(r'^blog/', include('application.blog.urls', app_name='blog', namespace='blog')),
    url(r'^admin/', admin.site.urls),
    url(r'^comment/(?P<app_label>\w+)/(?P<model>\w+)/(?P<pk>\d+)/$', CommentList.as_view(), name='comment.list'),
    url(r'^comment/create/$', CommentCreate.as_view(), name='comment.create'),
    url(r'^feedback/', include('application.feedback.urls', app_name='feedback', namespace='feedback')),
    url(r'^summernote/', include('django_summernote.urls')),
) + static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
+ static.static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# Now register i18n trees.
register_i18n_trees(['site'])

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [url(r'^__debug__/', include(debug_toolbar.urls))] + urlpatterns

@receiver(post_save, sender=Page)
def addUrlPatternOnAfterSave(sender, instance, **kwargs):
    global urlpatterns

    urlpatterns += [url(r'^(?P<url>%s)$' % instance.url, PageView.as_view())]