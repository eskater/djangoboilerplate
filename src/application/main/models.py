#coding: utf-8
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from django_summernote.models import AbstractAttachment

from .utils import get_modified_filename

class User(AbstractUser):
    picture = models.ForeignKey('File', null=True, blank=True, verbose_name=_(u'Изображение'))

    def save(self, *args, **kwargs):
        self.set_password(self.password)

        return super(self.__class__, self).save(*args, **kwargs)

class File(AbstractAttachment):
    file = models.FileField(upload_to=get_modified_filename, verbose_name=_(u'Файл'))
    name = models.CharField(max_length=100, null=True, blank=True, default='', verbose_name=_(u'Наименование'))
    size = models.IntegerField(default=0, null=True, blank=True, verbose_name=_(u'Размер файла'))
    mime = models.CharField(max_length=20, null=True, blank=True, default='none', verbose_name=_(u'Тип файла'))
    uploaded = models.DateTimeField(auto_now_add=True, null=True)
    date_modify = models.DateTimeField(auto_now=True, editable=False, verbose_name=_(u'Дата изменения'))
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_(u'Дата создания'))

    class Meta:
        verbose_name = _(u'Файл')
        verbose_name_plural = _(u'Файлы')

    def save(self, *args, **kwargs):
        if not self.pk and self.file:
            self.name = self.file.name
            self.size = self.file.size

        super(self.__class__, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if self.file:
            self.file.delete(*args, **kwargs)

        super(self.__class__, self).delete(*args, **kwargs)

    def __unicode__(self):
        return self.name or self.file.name

class Page(models.Model):
    url = models.CharField(max_length=255, null=True, blank=True, verbose_name=_(u'Ссылка'))
    title = models.CharField(max_length=30, verbose_name=_(u'Наименование'))
    content = models.TextField(verbose_name=_(u'Содержимое страницы'))
    date_modify = models.DateTimeField(auto_now=True, editable=False, verbose_name=_(u'Дата изменения'))
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_(u'Дата создания'))

    class Meta:
        verbose_name = _(u'Страница')
        verbose_name_plural = _(u'Страницы')

    def __unicode__(self):
        return self.title

class Comment(models.Model):
    text = models.TextField(verbose_name=_(u'Комментарий'))
    author = models.ForeignKey(User, null=True, on_delete=models.CASCADE, verbose_name=_(u'Автор'))
    date_modify = models.DateTimeField(null=True, auto_now=True, editable=False, verbose_name=_(u'Дата изменения'))
    date_create = models.DateTimeField(null=True, auto_now_add=True, verbose_name=_(u'Дата создания'))
    object_id = models.PositiveIntegerField(verbose_name=_(u'Идентификатор объекта'))
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, verbose_name=_(u'Тип объекта'))
    content_object = GenericForeignKey(for_concrete_model=True)

    class Meta:
        verbose_name = _(u'Комментарий')
        verbose_name_plural = _(u'Комментарии')

    def __unicode__(self):
        return self.text
