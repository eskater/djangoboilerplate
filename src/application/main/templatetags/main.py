#coding: utf-8
import json, bleach

from django import template
from django.utils.safestring import mark_safe
from django.template.defaultfilters import stringfilter

from .. import utils
from ..forms import *
from ..models import *

register = template.Library()

@register.simple_tag
def var(value):
	return value

@register.simple_tag
def resize(path, width, height):
	return utils.get_resize_picture(path, width, height)

@register.simple_tag
@stringfilter
def wysiwyg(value, **kwargs):
	tags = ['a', 'b', 'i', 'p', 'u', 'hr', 'ul', 'ol', 'br', 'img', 'strike']

	if kwargs.get('tags'):
		tags += kwargs.get('tags').split(' ')

	styles = ['width', 'height', 'float', 'text-align']

	if kwargs.get('styles'):
		styles += kwargs.get('styles').split(' ')

	attributes = {'a': ['href', 'target'], 'p': ['align', 'style'], 'img': ['src', 'style', 'align', 'width', 'height']}

	if kwargs.get('attributes'):
		attributes.update(json.loads(kwargs.get('attributes')))

	return mark_safe(bleach.clean(value, tags=tags, styles=styles, attributes=attributes))

@register.inclusion_tag('tags/pagination.html')
def pagination(**kwargs):
	return kwargs

@register.inclusion_tag('tags/comment_object.html', takes_context=True)
def comment_object(context, object, **kwargs):
	model = ContentType.objects.get_for_model(object)

	kwargs['form'] = CommentForm(initial={'object_id': object.pk, 'content_type': model.id})
	kwargs['list'] = Comment.objects.filter(object_id=object.pk, content_type__pk=model.id)
	kwargs['model'] = model
	kwargs['object'] = object
	kwargs['request'] = context['request']

	return kwargs