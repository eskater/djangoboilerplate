#coding: utf-8
import os, re, time, hashlib

from PIL import Image

from django.conf import settings
from django.utils.translation import get_language

def get_resize_picture(path, width, height, x=0, y=0, mode='RGB', color=(255, 255, 255, 255), quality=75, save_dir='resize'):
	name = ''.join(map(str, [path, width, height, x, y]))
	hash = hashlib.md5(name).hexdigest()
	
	try:
		format = re.findall(r'\w+$', path)[0]
		if not format:
			raise Exception('No format file')
		file = '%s.%s' % (hash, format)
	except IndexError:
		raise Exception('No format file')
		
	if not format in ['jpg', 'jpeg', 'png', 'gif']:
		raise Exception('Image format is not supported')
	
	path_root = os.path.join(settings.MEDIA_ROOT, path)
	save_path = os.path.join(settings.MEDIA_ROOT, save_dir, file)
	
	if not os.path.isfile(save_path):
		picture = Image.open(path_root)
		
		ratio = picture.size[0] / float(picture.size[1])
		rsize = int(width * ratio if width > height else height * ratio)
		
		picture.thumbnail((rsize, rsize), Image.ANTIALIAS)
		if not x and not y:
			x, y = ((height / 2 if index > 0 else width / 2) - size / 2 for index, size in enumerate(picture.size))
		
		canvas = Image.new(mode, (width, height), color)
		canvas.paste(picture, (x, y, x + picture.size[0], y + picture.size[1]))
		
		canvas.save(save_path, quality=quality)
		
	return os.path.join(settings.MEDIA_URL, save_dir, file)

def get_modified_filename(instance, filename):
	return 'storage/%s/%s' % (time.strftime('%Y/%m'), re.sub(r'^.*?(?=\.)', hashlib.md5(filename).hexdigest(), filename))

def get_current_language_code():
	return get_language().LANGUAGE_CODE