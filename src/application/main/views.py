#coding: utf-8
from django.views.generic import ListView, DetailView, CreateView, TemplateView

from forms import *
from models import *

class IndexView(TemplateView):
	template_name = 'index.html'

class PageView(DetailView):
	model = Page
	slug_field = 'url'
	slug_url_kwarg = 'url'
	template_name = 'page.html'

class CommentList(ListView):
    model = Comment
    template_name = 'comment/list.html'
    paginate_by = 5

    def get_queryset(self):
        return self.model.objects.filter(content_type__app_label=self.kwargs['app_label'], content_type__model=self.kwargs['model'], object_id=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        data = super(CommentList, self).get_context_data(**kwargs)

        return data

class CommentCreate(CreateView):
    model = Comment
    form_class = CommentForm
    template_name = 'comment/create.html'

    def form_valid(self, form):
        comment = form.save(commit=False)
        comment.author = self.request.user

        return super(CommentCreate, self).form_valid(form)

    def get_success_url(self):
        return self.request.POST.get('next')