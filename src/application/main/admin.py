# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from models import *

@admin.register(Page)
class UserAdmin(SummernoteModelAdmin):
    list_filter = ['date_create']
    list_display = ['id', 'url', 'title', 'date_modify', 'date_create']
    list_display_links = ['id', 'url', 'title']
    search_fields = ['title', 'content']

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_filter = ['groups', 'date_joined']
    list_display = ['id', 'username', 'email', 'first_name', 'last_name', 'date_joined']
    list_display_links = ['id', 'username']
    search_fields = ['username', 'email', 'first_name', 'last_name']

"""
@admin.register(File)
class EntryAdmin(admin.ModelAdmin):
    list_filter = ['mime', 'date_create']
    list_display = ['id', 'name', 'size', 'mime', 'date_create']
    list_display_links = ['id', 'name']
    search_fields = ['name']

    def get_category_list(self, instance):
        return ', '.join([category.title for category in instance.category.only('title')])

    get_category_list.admin_order_field = 'category__title'
    get_category_list.short_description = _(u'Категории')
"""

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['id', 'text', 'author', 'date_modify', 'date_create']
    search_fields = ['text']
