#coding: utf-8
from django import template

from ..models import Entry


register = template.Library()

@register.inclusion_tag('news/tags/sidebar_right_news.html')
def news_sidebar_right():
	return {'object_list': Entry.objects.all()[:2]}