#coding: utf-8
from django.conf.urls import url
from views import *

urlpatterns = [
    url(r'^(?P<slug>\w+)/$', NewsList.as_view(), name='index'),
    url(r'^(?P<slug>\w+)/(?P<pk>\d+)/$', NewsDetail.as_view(), name='detail'),
]