# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _

from application.main.models import File

class Category(models.Model):
    title = models.CharField(unique=True, max_length=30, verbose_name=_(u'Наименование'))
    slug = models.SlugField(unique=True, default='', max_length=30, verbose_name=_(u'Символьный код'))

    class Meta:
        verbose_name = _(u'Категория')
        verbose_name_plural = _(u'Категории')
        ordering = ['-title']

    def __unicode__(self):
        return self.title

class Entry(models.Model):
    title = models.CharField(max_length=100,  verbose_name=_(u'Наименование'))
    picture = models.ManyToManyField(File, blank=True, related_name='%(app_label)s_entries', verbose_name=_(u'Изображение'))
    detail_text = models.TextField(verbose_name=_(u'Детальный текст'))
    preview_text = models.TextField(verbose_name=_(u'Текст анонса'))
    category = models.ForeignKey(Category, null=True, related_name='entries', verbose_name=_(u'Категория'))
    date_modify = models.DateTimeField(auto_now=True, editable=False, verbose_name=_(u'Дата изменения'))
    date_create = models.DateTimeField(auto_now_add=True, verbose_name=_(u'Дата создания'))

    class Meta:
        verbose_name = _(u'Запись')
        verbose_name_plural = _(u'Записи')
        ordering = ['-date_create']

    def __unicode__(self):
        return self.title