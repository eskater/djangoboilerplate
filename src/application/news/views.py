# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic import ListView, DetailView

from models import *

class NewsList(ListView):
    template_name = 'news/index.html'

    def get_queryset(self):
        if self.kwargs.get('slug'):
            return Entry.objects.filter(category__slug=self.kwargs.get('slug'))

        return Entry.objects.all()

    def get_context_data(self, **kwargs):
        data = super(NewsList, self).get_context_data(**kwargs)

        if self.kwargs.get('slug'):
            data['category'] = Category.objects.get(slug=self.kwargs.get('slug'))

        return data

class NewsDetail(DetailView):
    model = Entry
    template_name = 'news/detail.html'

    def get_context_data(self, **kwargs):
        data = super(NewsDetail, self).get_context_data(**kwargs)

        if data.get('entry') and not data.get('category'):
            data['category'] = data.get('entry').category

        return data