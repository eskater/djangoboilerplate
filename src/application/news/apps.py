# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.apps import AppConfig

class NewsConfig(AppConfig):
    name = 'application.news'
    verbose_name = _(u'Новости')
