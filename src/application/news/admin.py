# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.contrib import admin

from models import *

@admin.register(Entry)
class EntryAdmin(admin.ModelAdmin):
    fields = ('title', ('preview_text', 'detail_text'), 'picture', 'category')
    list_filter = ['category', 'date_create']
    list_display = ['id', 'title', 'category', 'date_modify', 'date_create']
    list_display_links = ['id', 'title']
    search_fields = ['title', 'preview_text', 'detail_text']
    filter_horizontal = ['picture']

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}