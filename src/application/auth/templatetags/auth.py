#coding: utf-8
from django import template
from django.contrib.auth.forms import AuthenticationForm
register = template.Library()

@register.inclusion_tag('auth/tags/sidebar_right_login.html', takes_context=True)
def auth_sidebar_right_login(context):
	return {'form': AuthenticationForm(context['request']), 'request': context['request']}