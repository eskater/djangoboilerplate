#coding: utf-8
from django.conf.urls import include, url

from views import *

urlpatterns = [
    url(r'^$', LoginView.as_view(), name='index'),
    #url(r'^forgot/$', forgot, name='forgot'),
    #url(r'^change/$', change, name='change'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^register/$', RegisterView.as_view(), name='register'),
]