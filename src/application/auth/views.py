# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import views as auth_views
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView

from forms import *

from application.main.models import User

# Create your views here.
class LoginView(auth_views.LoginView):
    template_name = 'auth/index.html'
    redirect_authenticated_user = True

class LogoutView(auth_views.LogoutView):
    template_name = 'auth/logout.html'

class RegisterView(CreateView):
    model = User
    form_class = RegisterForm
    success_url = reverse_lazy('index')
    template_name = 'auth/register.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect('index')

        return super(RegisterView,self).get(request, *args, **kwargs)