#coding: utf-8
from django.utils.translation import ugettext_lazy as _
from django import forms

from application.main.models import User

class RegisterForm(forms.ModelForm):
    confirm_password = forms.CharField(widget=forms.PasswordInput, label=_(u'Еще раз пароль'))

    class Meta:
        model = User
        fields = ['username', 'email', 'password', 'confirm_password', 'first_name', 'last_name']
        widgets = {'password': forms.PasswordInput}

    def clean(self):
        data = super(RegisterForm, self).clean()

        password = data.get('password')
        confirm_password = data.get('confirm_password')

        if password != confirm_password:
            self.add_error('confirm_password', _(u'Пароли не совпадают'))