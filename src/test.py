import sys

def hell(func):
    def wrap(*args):
        return 'hell + %s' % func(args)

    return wrap

@hell
def test(value='yes'):
    return value

def heaven(cls):
    class Wrapper(cls):
        value = 'YES!'

        def say(self):
            print self.value + self.get_partion()

    return Wrapper

@heaven
class test2:
    value = 'no'

    def say(self):
        print self.value

    def get_partion(self):
        return 'YEAH!!'

if __name__ == '__main__':
    instance = test2()

    instance.say()
    print test(sys.argv[1])